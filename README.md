# AwesomeWm-config

This is my awesome configuration file.
![](https://gitlab.com/freezboltz/awesome/-/raw/main/wallpaper2.png)

# Install

### Linux and macOS
```sh
git clone https://gitlab.com/freezboltz/awesome.git ~/.config/
```
#### Submodules
NOTE: git submodule add ./lain/

##### clone this project including submodules
```sh
git clone https://gitlab.com/freezboltz/awesome.git ~/.config/
cd ~/.config/awesome
git submodule init
git submodule update
```
