return {
  bg_focus = '#333333',
  bg_minimize = '#47102a',
  bg_normal = '#000000',
  bg_systray = '#000000',
  bg_urgent = '#ff0000',
  border_focus = '#1177aa',
  border_marked = '#a72564',
  border_normal = '#333333',
  fg_focus = '#ffffff',
  fg_minimize = '#ffffff',
  fg_normal = '#ffffff',
  fg_urgent = '#ffffff',
}
