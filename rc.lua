-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.

---@diagnostic disable: undefined-global
pcall(require, 'luarocks.loader')

local lain = require 'lain'
-- Standard awesome library
local gears = require 'gears'
local awful = require 'awful'
require 'awful.autofocus'
-- Widget and layout library
local wibox = require 'wibox'
-- Theme handling library
local beautiful = require 'beautiful'
-- Notification library
local naughty = require 'naughty'
local menubar = require 'menubar'
local hotkeys_popup = require 'awful.hotkeys_popup'
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require 'awful.hotkeys_popup.keys'
local keybindings = require 'keybindings'
local clientkeys = require 'clientkeys'
local icons = require 'icons'

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
  naughty.notify {
    preset = { bg = '#000080', fg = '#ffffff', timeout = 0 },
    title = 'Oops, there were errors during startup!',
    text = awesome.startup_errors,
  }
end

-- Handle runtime errors after startup
do
  local in_error = false
  awesome.connect_signal('debug::error', function(err)
    -- Make sure we don't go into an endless error loop
    if in_error then
      return
    end
    in_error = true

    naughty.notify {
      preset = { bg = '#000080', fg = '#ffffff', timeout = 0 },
      title = 'Oops, an error happened!',
      text = tostring(err),
    }
    in_error = false
  end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
local theme_ok = pcall(function()
  local my_theme = string.format('%s/.config/awesome/theme.lua', os.getenv 'HOME')
  beautiful.init(my_theme)
end)

if not theme_ok then
  beautiful.init(gears.filesystem.get_themes_dir() .. 'default/theme.lua')
end

-- This is used later as the default terminal and editor to run.
local terminal = 'st'
local editor = os.getenv 'EDITOR' or 'vim'
local editor_cmd = terminal .. ' -e ' .. editor

-- Default super.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
local super = 'Mod4'

-- Table of layouts to cover with awful.layout.inc, order matters.
-- cycle through layouts using <super+space> [for next layouts] or <super + shift + space> [for previous layouts]
awful.layout.layouts = {
  awful.layout.suit.tile,
  awful.layout.suit.tile.left,
  awful.layout.suit.tile.bottom,
  awful.layout.suit.tile.top,
  awful.layout.suit.floating,
  awful.layout.suit.max,
  awful.layout.suit.max.fullscreen,
}
-- Auto start program
awful.spawn.with_shell '~/.config/awesome/autostart.sh'
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
local myawesomemenu = {
  {
    'Play with mpv ',
    function()
      awful.spawn.with_shell 'xclip -o | xargs -I{} mpv "{}"'
    end,
  },
  {
    'hotkeys',
    function()
      hotkeys_popup.show_help(nil, awful.screen.focused())
    end,
  },
  { 'Chrome', 'chromium' },
  { 'Incognito', 'chromium --incognito' },
  { 'brave', 'brave' },
  { 'Vivaldi', 'vivaldi' },
  { 'brave_p', 'brave --incognito' },
  { 'firefox', 'firefox' },
  { 'fire_p', 'firefox --private-window' },
  { 'Thunar', 'thunar' },
  { 'Thorium', 'thorium-browser' },
  { 'deluge', 'deluge-gtk' },
  { 'qbittorrent', 'qbittorrent' },
  { 'calculator', 'kcalc' },
  { 'manual', terminal .. ' -e man awesome' },
  { 'edit config', editor_cmd .. ' ' .. awesome.conffile },
  { 'restart', awesome.restart },
  {
    'quit',
    function()
      awesome.quit()
    end,
  },
}

local mymainmenu = awful.menu {
  items = { { 'awesome', myawesomemenu, beautiful.awesome_icon }, { 'open terminal', terminal } },
}

--[[
local mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
]]

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
-- local mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
local mytextclock = wibox.widget.textclock()

-- custom lain widgets
local text_sep = wibox.widget.textbox ' | '

local cpu = lain.widget.cpu {
  settings = function()
    widget:set_markup(' ' .. cpu_now.usage .. '% ')
  end,
}
-- lain volume widget
-- ALSA volume
local volicon = wibox.widget.imagebox()
local volume = lain.widget.alsa {
  settings = function()
    local index, perc = '', tonumber(volume_now.level) or 0
    local vlevel = volume_now.level

    if volume_now.status == 'off' then
      index = 'volmutedblocked'
      vlevel = ''
    else
      if perc <= 5 then
        index = 'volmuted'
        vlevel = vlevel .. '% '
      elseif perc <= 25 then
        index = 'vollow'
        vlevel = vlevel .. '% '
      elseif perc <= 75 then
        index = 'volmed'
        vlevel = vlevel .. '% '
      else
        index = 'volhigh'
        vlevel = vlevel .. '% '
      end
    end
    volicon:set_image(icons[index])
    widget:set_markup(vlevel)
  end,
}

local toggle_min_max_volume = function()
  local f = io.popen 'pamixer --get-volume'
  if f == nil then
    naughty.notify {
      preset = naughty.config.presets.critical,
      title = 'Volume fetch error',
      text = 'Getting volume status a nil value',
    }
    return
  end
  local volume_level = f:read()
  f:close()
  if volume_level == '100' then
    os.execute(string.format('%s set %s 0%%', volume.cmd, volume.channel))
    naughty.notify { title = 'Min Volume', text = 'Volume set to 0%' }
  else
    os.execute(string.format('%s set %s 100%%', volume.cmd, volume.channel))
    naughty.notify { title = 'Max Volume', text = 'Volume set to 100%' }
  end
end

volicon:buttons(gears.table.join(
  awful.button({}, 1, function()
    awful.spawn(string.format('%s -e alsamixer', terminal))
  end),
  awful.button({}, 2, function()
    toggle_min_max_volume()
  end),
  awful.button({}, 3, function()
    os.execute(string.format('%s set %s toggle', volume.cmd, volume.togglechannel or volume.channel))
    naughty.notify {
      title = 'Toggle Sound',
      text = 'Toggle Mute',
    }
  end),
  awful.button({}, 4, function()
    os.execute(string.format('%s set %s 1%%+', volume.cmd, volume.channel))
    naughty.notify {
      title = 'Increase sound',
      text = 'Increasing sound by 1%',
    }
  end),
  awful.button({}, 5, function()
    os.execute(string.format('%s set %s 1%%-', volume.cmd, volume.channel))
    naughty.notify {
      title = 'Decrease sound',
      text = 'Decreasing sound by 1%',
    }
  end)
))

-- MEM
local my_mem = lain.widget.mem {
  settings = function()
    widget:set_markup(' ' .. mem_now.used .. ' MB ')
  end,
}
-- Net
local my_net_down = lain.widget.net {
  settings = function()
    widget:set_markup(string.format('%06.1f', net_now.received) .. ' KB/s ')
  end,
}

local my_net_up = lain.widget.net {
  settings = function()
    widget:set_markup(string.format('%06.1f', net_now.sent) .. ' KB/s ')
  end,
}

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
  awful.button({}, 1, function(t)
    t:view_only()
  end),
  awful.button({ super }, 1, function(t)
    if client.focus then
      client.focus:move_to_tag(t)
    end
  end),
  awful.button({}, 3, awful.tag.viewtoggle),
  awful.button({ super }, 3, function(t)
    if client.focus then
      client.focus:toggle_tag(t)
    end
  end),
  awful.button({}, 4, function(t)
    awful.tag.viewnext(t.screen)
  end),
  awful.button({}, 5, function(t)
    awful.tag.viewprev(t.screen)
  end)
)

local tasklist_buttons = gears.table.join(
  awful.button({}, 1, function(c)
    if c == client.focus then
      c.minimized = true
    else
      c:emit_signal('request::activate', 'tasklist', { raise = true })
    end
  end),
  awful.button({}, 3, function()
    awful.menu.client_list { theme = { width = 250 } }
  end),
  awful.button({}, 4, function()
    awful.client.focus.byidx(1)
  end),
  awful.button({}, 5, function()
    awful.client.focus.byidx(-1)
  end)
)

local function set_wallpaper(s)
  -- Wallpaper
  if beautiful.wallpaper then
    local wallpaper = beautiful.wallpaper
    -- If wallpaper is a function, call it with the screen
    if type(wallpaper) == 'function' then
      wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, true)
  end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal('property::geometry', set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
  -- Wallpaper
  set_wallpaper(s)

  -- Each screen has its own tag table.
  awful.tag({ '1', '2', '3', '4', '5', '6', '7', '8', '9', '10' }, s, awful.layout.layouts[1])

  -- Create a promptbox for each screen
  s.mypromptbox = awful.widget.prompt()
  -- Create an imagebox widget which will contain an icon indicating which layout we're using.
  -- We need one layoutbox per screen.
  s.mylayoutbox = awful.widget.layoutbox(s)
  s.mylayoutbox:buttons(gears.table.join(
    awful.button({}, 1, function()
      awful.layout.inc(1)
    end),
    awful.button({}, 3, function()
      awful.layout.inc(-1)
    end),
    awful.button({}, 4, function()
      awful.layout.inc(1)
    end),
    awful.button({}, 5, function()
      awful.layout.inc(-1)
    end)
  ))
  -- Create a taglist widget
  s.mytaglist = awful.widget.taglist {
    screen = s,
    filter = awful.widget.taglist.filter.all,
    buttons = taglist_buttons,
  }

  -- Create a tasklist widget
  s.mytasklist = awful.widget.tasklist {
    screen = s,
    filter = awful.widget.tasklist.filter.currenttags,
    buttons = tasklist_buttons,
  }

  -- Create the wibox panel
  s.mywibox = awful.wibar { position = 'top', screen = s }

  -- Add widgets to the wibox
  s.mywibox:setup {
    layout = wibox.layout.align.horizontal,
    { -- Left widgets
      layout = wibox.layout.fixed.horizontal,
      --mylauncher,
      s.mytaglist,
      s.mypromptbox,
    },
    s.mytasklist, -- Middle widget
    { -- Right widgets
      layout = wibox.layout.fixed.horizontal,
      -- mykeyboardlayout,
      icons.net_down_icon,
      my_net_down.widget,
      icons.net_up_icon,
      my_net_up.widget,
      icons.memory,
      my_mem.widget,
      icons.cpu,
      cpu.widget,
      volicon,
      volume.widget,
      text_sep,
      wibox.widget.systray(),
      mytextclock,
      s.mylayoutbox,
    },
  }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
  awful.button({}, 3, function()
    mymainmenu:toggle()
  end),
  awful.button({}, 4, awful.tag.viewnext),
  awful.button({}, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
local globalkeys = keybindings
clientkeys = clientkeys
-- }}}

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 10 do
  globalkeys = gears.table.join(
    globalkeys,
    -- View tag only.
    awful.key({ super }, '#' .. i + 9, function()
      local screen = awful.screen.focused()
      local tag = screen.tags[i]
      if tag then
        tag:view_only()
      end
    end, { description = 'view tag #' .. i, group = 'tag' }),
    -- Toggle tag display.
    awful.key({ super, 'Control' }, '#' .. i + 9, function()
      local screen = awful.screen.focused()
      local tag = screen.tags[i]
      if tag then
        awful.tag.viewtoggle(tag)
      end
    end, { description = 'toggle tag #' .. i, group = 'tag' }),
    -- Move client to tag.
    awful.key({ super, 'Shift' }, '#' .. i + 9, function()
      if client.focus then
        local tag = client.focus.screen.tags[i]
        if tag then
          client.focus:move_to_tag(tag)
        end
      end
    end, { description = 'move focused client to tag #' .. i, group = 'tag' }),
    -- Toggle tag on focused client.
    awful.key({ super, 'Control', 'Shift' }, '#' .. i + 9, function()
      if client.focus then
        local tag = client.focus.screen.tags[i]
        if tag then
          client.focus:toggle_tag(tag)
        end
      end
    end, { description = 'toggle focused client on tag #' .. i, group = 'tag' })
  )
end

local clientbuttons = gears.table.join(
  awful.button({}, 1, function(c)
    c:emit_signal('request::activate', 'mouse_click', { raise = true })
  end),
  awful.button({ super }, 1, function(c)
    c:emit_signal('request::activate', 'mouse_click', { raise = true })
    awful.mouse.client.move(c)
  end),
  awful.button({ super }, 3, function(c)
    c:emit_signal('request::activate', 'mouse_click', { raise = true })
    awful.mouse.client.resize(c)
  end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
  -- All clients will match this rule.
  {
    rule = {},
    properties = {
      border_width = beautiful.border_width,
      border_color = beautiful.border_normal,
      callback = awful.client.setslave,
      focus = awful.client.focus.filter,
      raise = true,
      keys = clientkeys,
      buttons = clientbuttons,
      screen = awful.screen.preferred,
      placement = awful.placement.no_overlap + awful.placement.no_offscreen,
      size_hints_honor = false,
    },
  },

  -- Floating clients.
  {
    rule_any = {
      instance = {
        'copyq', -- Includes session name in class.
        'DTA', -- Firefox addon DownThemAll.
        'pinentry',
      },
      class = {
        'Arandr',
        'Blueman-manager',
        'Gpick',
        'Kruler',
        'MessageWin', -- kalarm.
        'Nsxiv', -- Neo Suckless X image viewer
        'Sxiv', -- Suckless X image viewer
        'Tor Browser', -- Needs a fixed window size to avoid fingerprinting by screen size.
        'veromix',
        'Wpa_gui',
        'xtightvncviewer',
      },

      -- Note that the name property shown in xprop might be set slightly after creation of the client
      -- and the name shown there might not match defined rules here.
      name = {
        'Event Tester', -- xev.
      },
      role = {
        'AlarmWindow', -- Thunderbird's calendar.
        'ConfigManager', -- Thunderbird's about:config.
        'pop-up', -- e.g. Google Chrome's (detached) Developer Tools.
      },
      type = 'dialog',
    },
    properties = {
      floating = true,
      placement = awful.placement.no_overlap + awful.placement.no_offscreen + awful.placement.centered,
    },
  },

  -- Add titlebars to normal clients and dialogs
  {
    rule_any = { type = { 'normal', 'dialog' } },
    properties = { titlebars_enabled = true },
  },

  {
    rule_any = { class = { 'Gpick', 'kcalc' }, instance = 'gpick' },
    properties = { floating = true, ontop = true },
  },

  {
    rule = { class = 'firefox', instance = 'Navigator' },
    except = { instance = 'Devtools' },
    properties = {
      floating = true,
      maximized_horizontal = true,
      maximized_vertical = true,
    },
  },

  {
    rule = { class = 'firefox', instance = 'Devtools' },
    properties = {
      floating = true,
      ontop = false,
    },
  },

  {
    rule = { class = 'code-oss' },
    properties = {
      floating = true,
      maximized_horizontal = true,
      maximized_vertical = true,
      switchtotag = true,
      tag = '7',
    },
  },

  {
    rule = { class = 'QjackCtl' },
    properties = { ontop = true, floating = true },
  },
  -- Set Xdman to always map on the tag named "7" on screen 1.
  {
    rule = { class = 'xdman-Main' },
    properties = { ontop = true, screen = 1, tag = '7', floating = true },
  },

  {
    rule = { class = 'kdenlive' },
    except = { type = 'dialog' },
    properties = {
      floating = true,
      maximized_horizontal = true,
      maximized_vertical = true,
    },
  },

  {
    rule = { class = 'Gimp', instance = 'gimp' },
    except = { type = 'dialog' },
    callback = function(c)
      c.border_width = 0
    end,
    properties = {
      floating = true,
      maximized_horizontal = true,
      maximized_vertical = true,
    },
  },

  {
    -- matches game client only
    rule = { class = 'cs2', instance = 'cs2' },
    properties = {
      callback = function(c)
        c.fullscreen = true
      end,
    },
  },

  {
    -- for picture in picture mode
    -- make client floating and sticky
    rule_any = {
      name = { 'Picture in picture', 'Picture-in-picture', 'Picture in Picture', 'Picture-in-Picture' },
    },
    properties = {
      ontop = true,
      floating = true,
      sticky = true,
      placement = function(c)
        awful.placement.bottom_right(c, { margins = { right = 20, bottom = 10 } })
        awful.placement.no_overlap(c)
      end,
      size_hints_honor = false,
      width = 480,
      height = 270,
    },
  },
  {
    rule = { class = 'Stopwatch' },
    properties = {
      ontop = true,
      floating = true,
      sticky = true,
      placement = function(c)
        awful.placement.bottom_right(c, { margins = { right = 30, bottom = 200 } })
        awful.placement.no_overlap(c)
      end,
      size_hints_honor = false,
      width = 290,
      height = 250,
    },
  },
  {
    rule = { class = 'Zathura' },
    properties = {
      border_width = 0,
      ontop = false,
      floating = true,
      placement = awful.placement.centered,
    },
  },

  {
    rule_any = { class = { 'mpv', 'MPlayer' } },
    properties = {
      border_width = 0,
      ontop = false,
      floating = true,
      placement = awful.placement.centered,
      maximized_vertical = function(c)
        return c:geometry().height > awful.screen.focused().geometry.height
      end,
      maximized_horizontal = function(c)
        return c:geometry().width > awful.screen.focused().geometry.width
      end,
    },
  },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal('manage', function(c)
  -- Set the windows at the slave,
  -- i.e. put it at the end of others instead of setting it master.
  -- if not awesome.startup then awful.client.setslave(c) end

  if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
    -- Prevent clients from being unreachable after screen count changes.
    awful.placement.no_offscreen(c)
  end
end)

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- Fullscreen setting for mpv
--
-- Function to toggle wibar visibility
local function toggle_wibar(c)
  --Get screen and check if it is the correct tag
  local s = c.screen
  local tag = c.first_tag
  if tag and tag.selected then
    for _, w in ipairs(s.wiboxes or { s.mywibox }) do
      w.visible = not (c.class == 'mpv' and tag.selected)
    end
  end
end

-- Hide wibar when mpv opens
client.connect_signal('manage', function(c)
  if c.class == 'mpv' then
    for s in screen do
      for _, w in ipairs(s.wiboxes or { s.mywibox }) do
        w.visible = true
      end
    end
  end
end)

-- Also toggle when switching focus
client.connect_signal("focus", toggle_wibar)
client.connect_signal("unfocus", toggle_wibar)

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal('request::titlebars', function(c)
  -- buttons for the titlebar
  local buttons = gears.table.join(
    awful.button({}, 1, function()
      c:emit_signal('request::activate', 'titlebar', { raise = true })
      awful.mouse.client.move(c)
    end),
    awful.button({}, 3, function()
      c:emit_signal('request::activate', 'titlebar', { raise = true })
      awful.mouse.client.resize(c)
    end)
  )

  awful.titlebar(c):setup {
    { -- Left
      awful.titlebar.widget.iconwidget(c),
      buttons = buttons,
      layout = wibox.layout.fixed.horizontal,
    },
    { -- Middle
      { -- Title
        align = 'center',
        widget = awful.titlebar.widget.titlewidget(c),
      },
      buttons = buttons,
      layout = wibox.layout.flex.horizontal,
    },
    { -- Right
      awful.titlebar.widget.floatingbutton(c),
      awful.titlebar.widget.maximizedbutton(c),
      awful.titlebar.widget.stickybutton(c),
      awful.titlebar.widget.ontopbutton(c),
      awful.titlebar.widget.closebutton(c),
      layout = wibox.layout.fixed.horizontal(),
    },
    layout = wibox.layout.align.horizontal,
  }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal('mouse::enter', function(c)
  c:emit_signal('request::activate', 'mouse_enter', { raise = false })
end)

client.connect_signal('focus', function(c)
  c.border_color = beautiful.border_focus
end)
client.connect_signal('unfocus', function(c)
  c.border_color = beautiful.border_normal
end)
-- }}}
