-- Standard awesome library
local gears = require 'gears'
local awful = require 'awful'

local super = 'Mod4'

local clientkeys = gears.table.join(
  awful.key({ super }, 'f', function(c)
    c.fullscreen = not c.fullscreen
    c:raise()
  end, { description = 'toggle fullscreen', group = 'client' }),
  awful.key({ super, 'Shift' }, 'c', function(c)
    c:kill()
  end, { description = 'close', group = 'client' }),
  awful.key({ super, 'Control' }, 'space', function(c)
    awful.client.floating.toggle(c)
  end, { description = 'toggle floating', group = 'client' }),
  awful.key({ super, 'Control' }, 'Return', function(c)
    c:swap(awful.client.getmaster())
  end, { description = 'move to master', group = 'client' }),
  awful.key({ super }, 'o', function(c)
    c:move_to_screen()
  end, { description = 'move to screen', group = 'client' }),
  awful.key({ super, 'Shift' }, 't', function(c)
    c.sticky = not c.sticky
  end, { description = 'toggle sticky client', group = 'client' }),
  awful.key({ super }, 't', function(c)
    c.ontop = not c.ontop
  end, { description = 'toggle keep on top', group = 'client' }),
  awful.key({ super }, 'n', function(c)
    -- The client currently has the input focus, so it cannot be
    -- minimized, since minimized clients can't have the focus.
    c.minimized = true
  end, { description = 'minimize', group = 'client' }),
  awful.key({ super }, 'm', function(c)
    c.maximized = not c.maximized
    c:raise()
  end, { description = '(un)maximize', group = 'client' }),
  awful.key({ super, 'Control' }, 'm', function(c)
    c.maximized_vertical = not c.maximized_vertical
    c:raise()
  end, { description = '(un)maximize vertically', group = 'client' }),
  awful.key({ super, 'Shift' }, 'm', function(c)
    c.maximized_horizontal = not c.maximized_horizontal
    c:raise()
  end, { description = '(un)maximize horizontally', group = 'client' })
)

return clientkeys
