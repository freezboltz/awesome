---@diagnostic disable: undefined-global
local gears = require 'gears'
local awful = require 'awful'
local hotkeys_popup = require 'awful.hotkeys_popup'

-- Theme handling library
local beautiful = require 'beautiful'

-- modkey variable
local super = 'Mod4'
local control = 'Control'

-- This is used later as the default terminal and editor to run.
local terminal = 'st'
local editor = os.getenv 'EDITOR' or 'vim'
local editor_cmd = terminal .. ' -e ' .. editor

local myawesomemenu = {
  {
    'hotkeys',
    function()
      hotkeys_popup.show_help(nil, awful.screen.focused())
    end,
  },
  { 'manual', terminal .. ' -e man awesome' },
  { 'edit config', editor_cmd .. ' ' .. awesome.conffile },
  { 'restart', awesome.restart },
  {
    'quit',
    function()
      awesome.quit()
    end,
  },
}

local mymainmenu = awful.menu {
  items = { { 'awesome', myawesomemenu, beautiful.awesome_icon }, { 'open terminal', terminal } },
}

local keybindings = gears.table.join(
  awful.key({ super }, 's', hotkeys_popup.show_help, { description = 'show help', group = 'awesome' }),
  awful.key({ super }, 'Left', awful.tag.viewprev, { description = 'view previous', group = 'tag' }),
  awful.key({ super }, 'Right', awful.tag.viewnext, { description = 'view next', group = 'tag' }),
  awful.key({ super }, 'Escape', awful.tag.history.restore, { description = 'go back to previous selected tag', group = 'tag' }),

  -- Switch client in workspace ny direction keys h j k l
  awful.key({ super }, 'h', function()
    awful.client.focus.global_bydirection 'left'
  end, { description = 'focus next window left', group = 'client' }),
  awful.key({ super }, 'j', function()
    awful.client.focus.global_bydirection 'down'
  end, { description = 'focus next window up', group = 'client' }),
  awful.key({ super }, 'k', function()
    awful.client.focus.global_bydirection 'up'
  end, { description = 'focus next window down', group = 'client' }),
  awful.key({ super }, 'l', function()
    awful.client.focus.global_bydirection 'right'
  end, { description = 'focus next window right', group = 'client' }),
  awful.key({ super }, 'w', function()
    mymainmenu:show()
  end, { description = 'show main menu', group = 'awesome' }),

  -- Layout manipulation
  awful.key({ super, 'Shift' }, 'j', function()
    awful.client.swap.byidx(1)
  end, { description = 'swap with next client by index', group = 'client' }),
  awful.key({ super, 'Shift' }, 'k', function()
    awful.client.swap.byidx(-1)
  end, { description = 'swap with previous client by index', group = 'client' }),
  awful.key({ super, 'Control' }, 'j', function()
    awful.screen.focus_relative(1)
  end, { description = 'focus the next screen', group = 'screen' }),
  awful.key({ super, 'Control' }, 'k', function()
    awful.screen.focus_relative(-1)
  end, { description = 'focus the previous screen', group = 'screen' }),
  awful.key({ super }, 'u', awful.client.urgent.jumpto, { description = 'jump to urgent client', group = 'client' }),
  awful.key({ super }, 'Tab', function()
    awful.client.focus.history.previous()
    if client.focus then
      client.focus:raise()
    end
  end, { description = 'go back', group = 'client' }),

  -- Standard program
  awful.key({ super, 'Control' }, 'r', awesome.restart, { description = 'reload awesome', group = 'awesome' }),
  awful.key({ super, 'Shift' }, 'q', awesome.quit, { description = 'quit awesome', group = 'awesome' }),
  awful.key({ super, 'Shift' }, 'h', function()
    awful.tag.incnmaster(1, nil, true)
  end, { description = 'increase the number of master clients', group = 'layout' }),
  awful.key({ super, 'Shift' }, 'l', function()
    awful.tag.incnmaster(-1, nil, true)
  end, { description = 'decrease the number of master clients', group = 'layout' }),
  awful.key({ super, 'Control' }, 'h', function()
    awful.tag.incncol(1, nil, true)
  end, { description = 'increase the number of columns', group = 'layout' }),
  awful.key({ super, 'Control' }, 'l', function()
    awful.tag.incncol(-1, nil, true)
  end, { description = 'decrease the number of columns', group = 'layout' }),
  awful.key({ super }, 'space', function()
    awful.layout.inc(1)
  end, { description = 'select next layout', group = 'layout' }),
  awful.key({ super, 'Shift' }, 'space', function()
    awful.layout.inc(-1)
  end, { description = 'select previous layout', group = 'layout' }),

  awful.key({ super, 'Control' }, 'n', function()
    local c = awful.client.restore()
    -- Focus restored client
    if c then
      c:emit_signal('request::activate', 'key.unminimize', { raise = true })
    end
  end, { description = 'restore minimized', group = 'client' }),

  -- My custom keybindings
  awful.key({ super }, 'x', function()
    awful.util.spawn 'archlinux-logout'
  end, { description = 'exit awesome session', group = 'hotkeys' }),
  awful.key({ super }, 'Escape', function()
    awful.util.spawn 'xkill'
  end, { description = 'Kill process', group = 'hotkeys' }),

  -- print
  awful.key({}, 'Print', function()
    awful.util.spawn "scrot 'ArcoLinux-%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'"
  end, { description = 'Scrot print_screen', group = 'screenshots' }),
  awful.key({ control }, 'Print', function()
    awful.util.spawn 'xfce4-screenshooter'
  end, { description = 'Xfce screenshot', group = 'screenshots' }),
  awful.key({ control, 'Shift' }, 'Print', function()
    awful.util.spawn 'gnome-screenshot -i'
  end, { description = 'Gnome screenshot', group = 'screenshots' })
)

return keybindings
