#!/bin/env bash
# This shebang line specifies the path to the bash interpreter.

# Define a function called 'run' to start a program if it is not already running.
function run {
    # Check if the program is already running by counting its process IDs (pgrep -c).
    if [ $(pgrep -c "$1") -eq 0 ]; then
        # If the program is not running (count is 0), start it in the background.
        "$@" &
    fi
}

# Use the 'run' function to start the network manager applet if not running.
run nm-applet
# Use the 'run' function to enable numlock if it is not already enabled.
run numlockx on
# Use the 'run' function to start sxhkd (Simple X HotKey Daemon) if not running.
run sxhkd
# Use the 'run' function to start flameshot (a screenshot tool) if not running.
run flameshot
run blueman-applet
run volctl

# The following lines are commented out. Uncomment them if you want these applications to start.

# Use the 'run' function to start volumeicon if not running.
# run volumeicon

# Use the 'run' function to start variety (a wallpaper changer) if not running.
# run variety

# Use the 'run' function to restore the previous wallpaper using nitrogen if not running.
# run nitrogen --restore

# Use the 'run' function to start pamac-tray (a package manager tray icon) if not running.
# run pamac-tray

# Use the 'run' function to start blueberry-tray (a Bluetooth tray icon) if not running.
# run blueberry-tray

# Use the 'run' function to start Polybar (a status bar) if not running.
# run "$HOME"/.config/polybar/polybar.sh

# The following lines can be used to start specific applications at startup.

# Use the 'run' function to start Firefox if not running.
# run firefox

# Use the 'run' function to start Atom (a text editor) if not running.
# run atom

# Use the 'run' function to start Dropbox if not running.
# run dropbox

# Use the 'run' function to start Insync (a Google Drive sync client) if not running.
# run insync start

# Use the 'run' function to start Spotify if not running.
# run spotify

# Use the 'run' function to start ckb-next (a Corsair keyboard driver) in the background if not running.
# run ckb-next -b

# Use the 'run' function to start Discord if not running.
# run discord

# Use the 'run' function to start Telegram Desktop if not running.
# run telegram-desktop
