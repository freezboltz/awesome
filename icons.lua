local wibox     = require("wibox")

local M = {}

local icon_folder = string.format("%s/.config/awesome/icons/", os.getenv("HOME"))

-- Volume Icons
M.volhigh                                   = icon_folder .. "/vol_high.png"
M.vollow                                    = icon_folder .. "/vol_low.png"
M.volmed                                    = icon_folder .. "/vol_medium.png"
M.volmutedblocked                           = icon_folder .. "/vol_muted_blocked.png"
M.volmuted                                  = icon_folder .. "/vol_muted.png"
M.voloff                                    = icon_folder .. "/vol_off.png"

M.net_up_icon = wibox.widget {
    image = icon_folder .. "net_up.png",
    resize = true,
    valign = "center",
    widget = wibox.widget.imagebox
}

M.net_down_icon = wibox.widget {
    image = icon_folder .. "net_down.png",
    resize = true,
    valign = "center",
    widget = wibox.widget.imagebox
}

M.memory = wibox.widget {
    image = icon_folder .. "mem.png",
    resize = true,
    valign = "center",
    widget = wibox.widget.imagebox
}

M.cpu = wibox.widget {
    image = icon_folder .. "cpu.png",
    resize = true,
    valign = "center",
    widget = wibox.widget.imagebox
}

return M
